import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.List;
import java.util.ArrayList;

public class JdbcMain {
  public static void main(String[] args) throws ClassNotFoundException {
    // TODO: Load the SQLite JDBC driver (JDBC class implements java.sql.Driver)
    
    Class.forName("org.sqlite.JDBC");
    // TODO: Create a DB connection
    try(Connection connection = DriverManager.getConnection("jdbc:sqlite:contactmgr.db")) {
      
      // TODO: Create a SQL statement
      Statement statement = connection.createStatement();
      
      // TODO: Create a DB table
      statement.executeUpdate("DROP TABLE IF EXISTS contacts");
      statement.executeUpdate("CREATE TABLE contacts (id INTEGER PRIMARY KEY, firstname STRING, lastname STRING, email STRING, phone INTEGER(11))");

     
      // TODO: Insert a couple contacts
      statement.executeUpdate("INSERT INTO contacts (firstname, lastname, email, phone) VALUES ('Tahmid', 'Choyon', 'tahmid.choyon@gmail.com', 01741778025)");
      statement.executeUpdate("INSERT INTO contacts (firstname, lastname, email, phone) VALUES ('John', 'Doe', 'jogndoe@gmail.com', 01688465250)");
      // TODO: Fetch all the records from the contacts table
      ResultSet rs = statement.executeQuery("SELECT * FROM contacts");      
      // TODO: Iterate over the ResultSet & display contact info
      while(rs.next()) {
        int id = rs.getInt("id");
        String firstname = rs.getString("firstname");
        String lastname = rs.getString("lastname");

        System.out.printf("(%d) %s %s\n", id, firstname, lastname);
      }
      
    } catch (SQLException ex) {
      // Display connection or query errors
      System.err.printf("There was a database error: %s%n",ex.getMessage());
    }
  }
}