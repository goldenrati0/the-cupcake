package info.tahmidchoyon.hibernatebasics;

import info.tahmidchoyon.hibernatebasics.model.Contact;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import java.util.List;

public class Application {

    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {

        final ServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        return new MetadataSources(registry).buildMetadata().buildSessionFactory();
    }

    public static void main(String[] args) {

        Contact contact = new Contact.ContactBuilder("Tahmid", "Choyon")
                .withEmail("tahmid.choyon@gmail.com")
                .withPhone(778025L)
                .build();
        save(contact);

        // Display all contacts
        getAllContacts().stream().forEach(System.out::println);
    }

    @SuppressWarnings("unchecked")
    private static List<Contact> getAllContacts() {

        // Open session
        Session session = sessionFactory.openSession();

        // Create a Criteria
        Criteria criteria = session.createCriteria(Contact.class);

        // Fetch all contacts based on Criteria
        List<Contact> contacts = criteria.list();

        // Close session
        session.close();

        return contacts;
    }

    private static void save(Contact contact) {
        // Open a session
        Session session = sessionFactory.openSession();

        // Begin transaction
        session.beginTransaction();

        // Save the contact in database
        session.save(contact);

        // Commit
        session.getTransaction().commit();

        // Close
        session.close();
    }
}
