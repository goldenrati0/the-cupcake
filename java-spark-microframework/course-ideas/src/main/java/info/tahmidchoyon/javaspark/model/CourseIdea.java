package info.tahmidchoyon.javaspark.model;

import com.github.slugify.Slugify;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CourseIdea {

    private String idea;
    private String author;
    private String slug;
    private Set<String> voters;

    public String getSlug() {
        return slug;
    }

    public List<String> getVoters() {
        return new ArrayList<>(voters);
    }

    public int getVoteCount() {
        return voters.size();
    }

    public CourseIdea(String idea, String author) {
        this.idea = idea;
        this.author = author;
        Slugify slugify = new Slugify();
        this.slug = slugify.slugify(idea);
        this.voters = new HashSet<>();
    }

    public String getIdea() {
        return idea;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CourseIdea that = (CourseIdea) o;

        if (idea != null ? !idea.equals(that.idea) : that.idea != null) return false;
        return author != null ? author.equals(that.author) : that.author == null;
    }

    @Override
    public int hashCode() {
        int result = idea != null ? idea.hashCode() : 0;
        result = 31 * result + (author != null ? author.hashCode() : 0);
        return result;
    }

    public boolean addVote(String username) {
        return voters.add(username);
    }
}
