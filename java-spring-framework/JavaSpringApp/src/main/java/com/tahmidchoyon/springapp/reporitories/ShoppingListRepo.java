package com.tahmidchoyon.springapp.reporitories;

import com.tahmidchoyon.springapp.models.ShoppingList;

import java.util.ArrayList;
import java.util.List;

public class ShoppingListRepo {

    public List<ShoppingList> shoppingLists;

    public ShoppingListRepo() {
        this.shoppingLists = new ArrayList<>();

        this.shoppingLists.add(
                new ShoppingList(1, "Walmart")
        );
        this.shoppingLists.add(
                new ShoppingList(2, "Amazon")
        );
        this.shoppingLists.add(
                new ShoppingList(3, "IKEA")
        );
    }

}
