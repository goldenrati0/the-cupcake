package com.tahmidchoyon.springapp.controllers;

import com.tahmidchoyon.springapp.models.ShoppingList;
import com.tahmidchoyon.springapp.services.ShoppingListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class ShoppingListController {

    @Autowired
    public ShoppingListService shoppingListService;

    @RequestMapping("/list")
    @ResponseStatus(HttpStatus.OK)
    public List<ShoppingList> getAllShoppingList() {
        return shoppingListService.getAllShoppingList();
    }

    @RequestMapping("/list/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ShoppingList findById(@PathVariable int id) {
        return shoppingListService.findShoppingList(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/list")
    @ResponseStatus(HttpStatus.CREATED)
    public ShoppingList createShoppingList(@RequestBody ShoppingList shoppingList) {
        shoppingListService.addShoppingList(shoppingList);
        return shoppingList;
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/list/{id}", consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public ShoppingList updateShoppingList(@PathVariable int id, @RequestBody Map<String, String> payload) {
        ShoppingList shoppingList = shoppingListService.findShoppingList(id);
        shoppingList.setTitle(payload.getOrDefault("title", "N/A"));
        return shoppingList;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/list/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteShoppingList(@PathVariable int id) {
        shoppingListService.deleteShoppingList(id);
    }
}
