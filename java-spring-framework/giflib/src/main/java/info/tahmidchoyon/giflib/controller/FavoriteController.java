package info.tahmidchoyon.giflib.controller;

import info.tahmidchoyon.giflib.data.GifRepository;
import info.tahmidchoyon.giflib.model.Gif;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class FavoriteController {

    @Autowired
    private GifRepository gifRepository;

    @RequestMapping("/favorites")
    public String favorites(ModelMap modelMap) {

        List<Gif> favoriteGifs = gifRepository.favoriteGifs();
        modelMap.put("gifs", favoriteGifs);
        
        return "favorites";
    }
}
