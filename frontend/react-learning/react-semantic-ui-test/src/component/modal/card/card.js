import React from 'react'
import {Card, Icon} from 'semantic-ui-react'

class CustomCard extends React.Component {
    render() {
        return (
            <Card
                image={this.props.avatar}
                header={this.props.name}
                meta={this.props.date}
                description={this.props.description}
                extra={
                    <a href={"#"}>
                        <Icon name='star'/>
                        {this.props.rating}
                    </a>
                }
            />
        )
    }
}

export default CustomCard;