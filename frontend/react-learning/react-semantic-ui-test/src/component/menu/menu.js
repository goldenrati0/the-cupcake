import React from 'react'
import {Input, Menu} from 'semantic-ui-react'

export default class CustomMenu extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            activeItem: 'home'
        };

        this.handleItemClick = (e, {name}) => this.setState({activeItem: name});
    }


    render() {
        const {activeItem} = this.state;

        return (
            <div className="container">
                <Menu secondary>

                    {this.props.menuItems.map(item => {
                        return (
                            <Menu.Item name={item.name} active={activeItem === item.name}
                                       onClick={this.handleItemClick}/>
                        )
                    })}

                    <Menu.Menu position='right'>
                        <Menu.Item>
                            <Input icon='search' placeholder='Search...'/>
                        </Menu.Item>
                    </Menu.Menu>

                </Menu>
            </div>
        )
    }
}