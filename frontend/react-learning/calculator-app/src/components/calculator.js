import React from 'react'
import CalculatorTitle from "./calculatorTitle";
import OutputScreen from "./outputScreen";
import Button from "./button";

class Calculator extends React.Component {

    constructor() {
        super();

        this.state = {
            expression: '',
            answer: ''
        };

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event) {

        const val = event.target.value;

        switch (val) {
            case '=': {
                if (this.state.expression !== '') {
                    var answer = '';
                    try {
                        answer = eval(this.state.expression);
                        console.log(answer);
                        this.setState({
                            expression: '',
                            answer: answer
                        });

                    } catch (err) {
                        this.setState({
                            expression: '',
                            answer: 'Math Error'
                        })
                    }
                }
                break;
            }

            case 'Clear': {
                this.setState({
                    expression: '',
                    answer: ''
                });
                break;
            }

            case 'Delete': {
                var expression = this.state.expression;
                expression = expression.substr(0, expression.length - 1);
                this.setState({
                    expression: expression
                });
                break;
            }

            default: {


                if ((val === '+' || val === '-' || val === '*' || val === '/') && this.state.expression === '') {
                    if (this.state.answer !== '' || this.state.answer !== null) {
                        this.setState({
                            expression: this.state.answer + val,
                            answer: ''
                        })
                    }
                } else {
                    this.setState((prevState, props) => ({expression: prevState.expression + val}));
                }
            }
        }
    }

    render() {
        return (
            <div className="frame">

                <CalculatorTitle value={"React Calculator"}/>

                <div className="mainCalc">

                    <OutputScreen {...this.state} />

                    <div className="button-row">
                        <Button label={'Clear'} handleClick={this.handleClick}/>
                        <Button label={'Delete'} handleClick={this.handleClick}/>
                        <Button label={'.'} handleClick={this.handleClick}/>
                        <Button label={'/'} handleClick={this.handleClick}/>
                    </div>

                    <div className="button-row">
                        <Button label={'7'} handleClick={this.handleClick}/>
                        <Button label={'8'} handleClick={this.handleClick}/>
                        <Button label={'9'} handleClick={this.handleClick}/>
                        <Button label={'*'} handleClick={this.handleClick}/>
                    </div>

                    <div className="button-row">
                        <Button label={'4'} handleClick={this.handleClick}/>
                        <Button label={'5'} handleClick={this.handleClick}/>
                        <Button label={'6'} handleClick={this.handleClick}/>
                        <Button label={'-'} handleClick={this.handleClick}/>
                    </div>

                    <div className="button-row">
                        <Button label={'1'} handleClick={this.handleClick}/>
                        <Button label={'2'} handleClick={this.handleClick}/>
                        <Button label={'3'} handleClick={this.handleClick}/>
                        <Button label={'+'} handleClick={this.handleClick}/>
                    </div>

                    <div className="button-row">
                        <Button label={'0'} handleClick={this.handleClick}/>
                        <Button label={'='} handleClick={this.handleClick}/>
                    </div>

                </div>
            </div>
        )
    }
}

export default Calculator;