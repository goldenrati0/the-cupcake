from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.mysql import LONGTEXT
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:letsplay@localhost:3306/pobasic'
app.config['SECRET_KEY'] = 'po_dummy_secret_key'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class Questions(db.Model):
    __tablename__ = 'questions'
    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    body = db.Column('body', LONGTEXT, nullable=False)
    total_options = db.Column('total_options', db.Integer)
    added = db.Column('added', db.DateTime)
    options = db.relationship('Options', cascade='all, delete-orphan', backref='of', lazy='dynamic')

    def __init__(self, body, total_options=4):
        self.body = body
        self.total_options = total_options
        self.added = datetime.now()

    def json_dump(self):
        return {
            "id": self.id,
            "body": self.body
        }


class Options(db.Model):
    __tablename__ = 'options'
    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    question_id = db.Column(db.Integer, db.ForeignKey('questions.id'))
    text = db.Column('text', LONGTEXT, nullable=False)
    is_correct = db.Column('is_correct', db.Boolean)

    def __init__(self, question_id, text, is_correct=False):
        self.question_id = question_id
        self.text = text
        self.is_correct = is_correct
