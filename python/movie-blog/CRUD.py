from databaseModel import db, user, movies, wishlist, imdb_movies


def register_user(name, email, password, phone=None):
    new_user = user(name, email, password, phone=phone)
    db.session.add(new_user)
    db.session.commit()


def add_movie(user_id, imdb_id):
    new_movie = movies(user_id, imdb_id)
    db.session.add(new_movie)
    db.session.commit()


def add_imdb_movie(imdb_id):
    if not search_imdb_movie(id=imdb_id):
        new_movie = imdb_movies(imdb_id=imdb_id)
        if new_movie.imdb_id is None:
            print("Not properly formatted: " + imdb_id)
            return None
        print("Movie added for IMDB ID: " + imdb_id)
        db.session.add(new_movie)
        db.session.commit()
    else:
        print("Already in database: " + imdb_id)


def search_imdb_movie(id):
    mov = imdb_movies.query.filter_by(imdb_id=id).first()
    if mov is None:
        return False
    return True


def delete_movie(user_id, imdb_id):
    del_movie = movies.query.filter_by(user_id=user_id, imdb_id=imdb_id).first()
    db.session.delete(del_movie)
    db.session.commit()


def add_wishlist(user_id, imdb_id):
    if hasMovie(user_id, imdb_id):
        return "You have already watched this movie!"
    else:
        new_wishlist = wishlist(user_id, imdb_id)
        db.session.add(new_wishlist)
        db.session.commit()


def delete_wishlist(user_id, imdb_id):
    del_movie = wishlist.query.filter_by(user_id=user_id, imdb_id=imdb_id).first()
    db.session.delete(del_movie)
    db.session.commit()


def getID(email):
    find_user = user.query.filter_by(email=email).first()
    return find_user.id


def reset_datbase():
    db.create_all()


def hasMovie(user_id, imdb_id):
    m = movies.query.filter_by(user_id=user_id, imdb_id=imdb_id).first()
    if m == None:
        return False
    return True


def switch(user_id, imdb_id):
    m = movies.query.filter_by(user_id=user_id, imdb_id=imdb_id).first()
    w = wishlist.query.filter_by(user_id=user_id, imdb_id=imdb_id).first()

    if w == None:
        delete_movie(user_id, imdb_id)
        add_wishlist(user_id, imdb_id)
    elif m == None:
        add_movie(user_id, imdb_id)
        delete_wishlist(user_id, imdb_id)
