from flask import Flask, render_template, url_for, request, session, flash, redirect, abort
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import requests
from util import getTrailer

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:letsplay@localhost:3306/movie_database'
db = SQLAlchemy(app)


class user(db.Model):
    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    email = db.Column('email', db.String(100), unique=True)
    password = db.Column('password', db.Text)
    name = db.Column('name', db.Text)
    phone = db.Column('phone', db.Text)
    movies = db.relationship('movies', backref='watched', lazy='dynamic')
    wishlist = db.relationship('wishlist', backref='wishlisted', lazy='dynamic')
    regtime = db.Column(db.DateTime)

    def __init__(self, name, email, password, phone=None):
        self.name = name
        self.email = email
        self.password = password
        self.phone = phone
        self.regtime = datetime.now()


class movies(db.Model):
    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    imdb_id = db.Column('imdb_id', db.String(50))
    name = db.Column('name', db.String(100))
    release_year = db.Column('release_year', db.Integer)
    imdb_rating = db.Column('imdb_rating', db.Float)
    genre = db.Column('genre', db.Text)
    director = db.Column('director', db.Text)
    actors = db.Column('actors', db.Text)
    poster_url = db.Column('poster_url', db.Text)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    add_time = db.Column(db.DateTime)

    def getURL(self):
        return getTrailer(self.name + " (" + str(self.release_year) + ")")

    def __init__(self, user_id, imdb_id):
        info = requests.get(url='http://www.omdbapi.com/?i=' + imdb_id)
        load_movie_info = info.json()

        self.name = load_movie_info['Title']
        self.imdb_id = imdb_id
        self.release_year = load_movie_info['Year']
        self.imdb_rating = load_movie_info['imdbRating']
        self.genre = load_movie_info['Genre']
        self.director = load_movie_info['Director']
        self.actors = load_movie_info['Actors']
        self.poster_url = load_movie_info['Poster']
        self.add_time = datetime.now()
        self.user_id = user_id


class wishlist(db.Model):
    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    imdb_id = db.Column('imdb_id', db.String(50))
    name = db.Column('name', db.String(100))
    release_year = db.Column('release_year', db.Integer)
    imdb_rating = db.Column('imdb_rating', db.Float)
    genre = db.Column('genre', db.Text)
    director = db.Column('director', db.Text)
    actors = db.Column('actors', db.Text)
    poster_url = db.Column('poster_url', db.Text)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    add_time = db.Column(db.DateTime)

    def __init__(self, user_id, imdb_id):
        info = requests.get(url='http://www.omdbapi.com/?i=' + imdb_id)
        load_movie_info = info.json()

        self.name = load_movie_info['Title']
        self.imdb_id = imdb_id
        self.release_year = load_movie_info['Year']
        self.imdb_rating = load_movie_info['imdbRating']
        self.genre = load_movie_info['Genre']
        self.director = load_movie_info['Director']
        self.actors = load_movie_info['Actors']
        self.poster_url = load_movie_info['Poster']
        self.add_time = datetime.now()
        self.user_id = user_id


class imdb_movies(db.Model):
    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    imdb_id = db.Column('imdb_id', db.String(50), unique=True)
    title = db.Column('title', db.String(100))
    year = db.Column('year', db.Integer)
    rated = db.Column('rated', db.Text)
    release_date = db.Column('release_date', db.Text)
    runtime = db.Column('runtime', db.Text)
    genre = db.Column('genre', db.Text)
    director = db.Column('director', db.Text)
    writer = db.Column('writer', db.Text)
    actors = db.Column('actors', db.Text)
    language = db.Column('language', db.Text)
    country = db.Column('country', db.Text)
    awards = db.Column('awards', db.Text)
    poster = db.Column('poster', db.Text)
    imdb_rating = db.Column('imdb_rating', db.Float)
    imdb_votes = db.Column('imdb_votes', db.Text)
    rotten_tomatoes = db.Column('rotten_tomatoes', db.Text)
    metascore = db.Column('metascore', db.Text)
    production = db.Column('production', db.Text)

    def getURL(self):
        return getTrailer(self.title + " (" + str(self.year) + ")")

    def __init__(self, imdb_id):
        movie_json = requests.get(url='http://www.omdbapi.com/?i=' + imdb_id)
        movie_info = movie_json.json()

        if not movie_info['Response']:
            return None
        if 'Season' in movie_info:
            return None
        if 'Episode' in movie_info:
            return None
        if 'Production' not in movie_info:
            return None
        if len(movie_info['Ratings']) == 0:
            return None
        if len(movie_info['Ratings']) == 1:
            return None
        if len(movie_info['Ratings']) == 2:
            if not movie_info['Ratings'][1]['Source'] == 'Rotten Tomatoes':
                return None

        self.imdb_id = imdb_id
        self.title = movie_info['Title']
        self.year = movie_info['Year']
        self.rated = movie_info['Rated']
        self.release_date = movie_info['Released']
        self.runtime = movie_info['Runtime']
        self.genre = movie_info['Genre']
        self.director = movie_info['Director']
        self.writer = movie_info['Writer']
        self.actors = movie_info['Actors']
        self.language = movie_info['Language']
        self.country = movie_info['Country']
        self.awards = movie_info['Awards']
        self.poster = movie_info['Poster']
        self.imdb_rating = movie_info['imdbRating']
        self.imdb_votes = movie_info['imdbVotes']
        self.rotten_tomatoes = movie_info['Ratings'][1]['Value']
        self.metascore = movie_info['Metascore']
        self.production = movie_info['Production']
