import urllib
import urllib.parse
from urllib.request import urlopen
import requests
from bs4 import BeautifulSoup


def getTrailer(search_string):
    query = urllib.parse.quote(search_string + " official trailer")
    url = "https://www.youtube.com/results?search_query=" + query
    response = urlopen(url)
    html = response.read()
    soup = BeautifulSoup(html)
    for vid in soup.findAll(attrs={'class': 'yt-uix-tile-link'}):
        return ('https://www.youtube.com/embed/' + vid['href'][9:])


def imdbid_from_name(search_string):
    movie_json = requests.get(url='http://www.omdbapi.com/?t=' + search_string)
    movie_info = movie_json.json()
    if 'Error' not in movie_info:
        return movie_info['imdbID']
    else:
        return None
