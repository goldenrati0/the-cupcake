autopep8==1.3.5
certifi==2018.4.16
Django==2.0.4
pycodestyle==2.4.0
pytz==2018.4
wincertstore==0.2
