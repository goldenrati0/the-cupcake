import csv
from datetime import datetime

ERROR_FILE_NAME = 'data/errors/error-' + str(datetime.now()).replace(' ', '-') + '.csv'
ERROR_FIELDNAMES = ['name', 'phone']

SUCCESS_FILE_NAME = 'data/users/user-' + str(datetime.now()).replace(' ', '-') + '.csv'
SUCCESS_FIELDNAMES = ['name', 'phone', 'sms']


def create_error_file_writer():
    error_file_writer = csv.DictWriter(open(ERROR_FILE_NAME, 'a'), fieldnames=ERROR_FIELDNAMES)
    return error_file_writer


def create_success_file_writer():
    success_file_writer = csv.DictWriter(open(SUCCESS_FILE_NAME, 'a'), fieldnames=SUCCESS_FIELDNAMES)
    return success_file_writer


def write_errors(error_file_writer, user):
    error_file_writer.writerow(user)


def write_users(success_file_writer, user):
    success_file_writer.writerow(user)


with open('data/data.csv') as csvfile:
    # create reader to read CSV file
    reader = csv.reader(csvfile, delimiter=',')

    # create error-{current_time}.csv
    error_file_writer = create_error_file_writer()
    # writing row headers
    error_file_writer.writeheader()
    # create user-{current_time}.csv
    success_file_writer = create_success_file_writer()
    # writing row headers
    success_file_writer.writeheader()

    error = 0
    success = 0

    for row in reader:
        if row[1] != 'phone':
            name = row[0]
            phone = row[1]
            sms = name + ', this is a test SMS. Unless you owe me money, ignore it!'
            if len(phone) >= 10:
                phone = phone[-10:]
                phone = '+880' + phone
                user = {'name': name, 'phone': phone, 'sms': sms}
                write_users(success_file_writer, user)
                success += 1
            else:
                user = {'name': name, 'phone': phone}
                write_errors(error_file_writer, user)
                error += 1
    print("Total SMS send: {}\nErrors count: {}".format(success, error))
