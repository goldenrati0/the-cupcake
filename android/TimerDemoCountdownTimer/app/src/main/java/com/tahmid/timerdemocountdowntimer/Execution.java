package com.tahmid.timerdemocountdowntimer;

import android.os.CountDownTimer;
import android.widget.TextView;

/**
 * Created by tahmid on 4/27/17.
 */

public class Execution extends CountDownTimer {

    private TextView textView;

    /**
     * @param millisInFuture    The number of millis in the future from the call
     *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
     *                          is called.
     * @param countDownInterval The interval along the way to receive
     *                          {@link #onTick(long)} callbacks.
     */
    public Execution(long millisInFuture, long countDownInterval, TextView textView) {
        super(millisInFuture, countDownInterval);
        this.textView = textView;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        textView.setText((millisUntilFinished/1000) + " seconds left!");
    }

    @Override
    public void onFinish() {
        textView.setText("Countdown finished!");
    }
}
