package com.tahmid.eggtimer;

import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private SeekBar seekBar;
    private TextView timerTextView;
    private Button timerButton;
    private ImageView imageView;
    MediaPlayer airhornPlayer;
    CountDownTimer countDownTimer;
    boolean timerIsActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seekBar = (SeekBar) findViewById(R.id.seekBar);
        timerTextView = (TextView) findViewById(R.id.timerTextView);
        timerButton = (Button) findViewById(R.id.timerButton);
        imageView = (ImageView) findViewById(R.id.imageView);
        airhornPlayer = MediaPlayer.create(getApplicationContext(), R.raw.airhorn);

        seekBar.setMax(600);
        timerTextView.setText("0:30");
        seekBar.setProgress(30);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                setTimerTextView(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void startTimer(View view) {

        if (!timerIsActive) {

            seekBar.setEnabled(false);
            timerButton.setText("Stop");
            timerIsActive = true;

            countDownTimer = new CountDownTimer(seekBar.getProgress() * 1000 + 100, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    setTimerTextView((long) millisUntilFinished / 1000);
                }

                @Override
                public void onFinish() {
                    airhornPlayer.start();
                    resetTimer();
                }
            }.start();
        } else {

            countDownTimer.cancel();
            resetTimer();
        }
    }

    public void setTimerTextView(long progress) {

        int minutes = (int) (progress / 60);
        int seconds = (int) (progress % 60);
        String secondString = (seconds <= 9) ? String.valueOf("0" + seconds) : String.valueOf(seconds);

        timerTextView.setText(minutes + ":" + secondString);
        seekBar.setProgress((int) progress);
    }

    public void resetTimer() {
        seekBar.setEnabled(true);
        timerButton.setText("Go!");
        timerIsActive = false;
        seekBar.setProgress(30);
        timerTextView.setText("0:30");
    }
}
