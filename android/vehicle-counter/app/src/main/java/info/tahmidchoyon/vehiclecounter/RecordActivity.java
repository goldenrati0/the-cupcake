package info.tahmidchoyon.vehiclecounter;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.sdsmdg.tastytoast.TastyToast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.tahmidchoyon.vehiclecounter.model.Record;

public class RecordActivity extends AppCompatActivity {

    @BindView(R.id.recordScrollView)
    ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);
        ButterKnife.bind(this);

        ArrayList<Record> recordArrayList;

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            recordArrayList = extras.getParcelableArrayList("data");
            if (recordArrayList != null) {
                TextView textView = new TextView(this);
                if (recordArrayList.size() > 0) {
                    StringBuilder stringBuilder = new StringBuilder("");
                    for (int i = recordArrayList.size() - 1; i >= 0; i--) {
                        stringBuilder.append(recordArrayList.get(i).toString());
                    }
                    textView.setText(stringBuilder.toString());
                } else {
                    textView.setText("No Data");
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                }
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
                scrollView.addView(textView, params);
            } else {
                TastyToast.makeText(getApplicationContext(), "No Data", TastyToast.LENGTH_LONG, TastyToast.ERROR);
            }
        }
    }
}
