package info.tahmidchoyon.vehiclecounter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.sdsmdg.tastytoast.TastyToast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.tahmidchoyon.vehiclecounter.database.DatabaseOperations;
import info.tahmidchoyon.vehiclecounter.model.Record;

public class CounterActivity extends AppCompatActivity {

    @BindView(R.id.dateTextView)
    TextView dateTextView;
    @BindView(R.id.busCounterTextView)
    TextView busCounterTextView;
    @BindView(R.id.truckCounterTextView)
    TextView truckCounterTextView;
    @BindView(R.id.carCounterTextView)
    TextView carCounterTextView;
    @BindView(R.id.bikeCounterTextView)
    TextView bikeCounterTextView;
    @BindView(R.id.cngCounterTextView)
    TextView cngCounterTextView;
    @BindView(R.id.cycleCounterTextView)
    TextView cycleCounterTextView;
    @BindView(R.id.rickshawCounterTextView)
    TextView rickshawCounterTextView;

    String startTime;
    String endTime;
    DatabaseOperations databaseOperations;

    private int bus, truck, car, bike, cng, cycle, rickshaw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counter);
        ButterKnife.bind(this);

        dateTextView.setText(new SimpleDateFormat("dd MMMM yyyy").format(Calendar.getInstance().getTime()));
        startTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Calendar.getInstance().getTime());
        databaseOperations = DatabaseOperations.getInstance(this);

        bus = 0;
        truck = 0;
        car = 0;
        bike = 0;
        cng = 0;
        cycle = 0;
        rickshaw = 0;
    }

    public void onSaveButtonClick(View view) {
        endTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Calendar.getInstance().getTime());
        databaseOperations.saveData(bus, truck, car, bike, cng, cycle, rickshaw, dateTextView.getText().toString(), startTime, endTime);
        TastyToast.makeText(getApplicationContext(), "Data Saved", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
        resetCounters();
    }

    public void onShowButtonClick(View view) {
        ArrayList<Record> recordList = databaseOperations.retrieveData();
        Intent intent = new Intent(this, RecordActivity.class);
        intent.putParcelableArrayListExtra("data", recordList);
        startActivity(intent);
    }

    public void resetCounters() {
        this.bus = 0;
        this.truck = 0;
        this.car = 0;
        this.bike = 0;
        this.cng = 0;
        this.cycle = 0;
        this.rickshaw = 0;
        this.startTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Calendar.getInstance().getTime());

        busCounterTextView.setText(String.valueOf(0));
        truckCounterTextView.setText(String.valueOf(0));
        carCounterTextView.setText(String.valueOf(0));
        bikeCounterTextView.setText(String.valueOf(0));
        cngCounterTextView.setText(String.valueOf(0));
        cycleCounterTextView.setText(String.valueOf(0));
        rickshawCounterTextView.setText(String.valueOf(0));
    }

    public void onEraseButtonClick(View view) {
        new MaterialDialog.Builder(this)
                .title("Enter Password")
                .content("To erase database, verify yourself!")
                .inputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_PASSWORD)
                .input("Password", "", false, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                        if (input.toString().equals("123456")) {
                            databaseOperations.deleteData();
                            resetCounters();
                            TastyToast.makeText(getBaseContext(), "Erased!", TastyToast.LENGTH_SHORT, TastyToast.INFO);
                        } else {
                            TastyToast.makeText(getBaseContext(), "Wrong passcode!", TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                        }
                    }
                }).show();
    }

    public void onBusIconClick(View view) {
        busCounterTextView.setText(String.valueOf(++bus));
    }

    public void onTruckIconClick(View view) {
        truckCounterTextView.setText(String.valueOf(++truck));
    }

    public void onCarIconClick(View view) {
        carCounterTextView.setText(String.valueOf(++car));
    }

    public void onBikeIconClick(View view) {
        bikeCounterTextView.setText(String.valueOf(++bike));
    }

    public void onCngIconClick(View view) {
        cngCounterTextView.setText(String.valueOf(++cng));
    }

    public void onCycleIconClick(View view) {
        cycleCounterTextView.setText(String.valueOf(++cycle));
    }

    public void onRickshawIconClick(View view) {
        rickshawCounterTextView.setText(String.valueOf(++rickshaw));
    }
}
