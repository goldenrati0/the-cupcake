package tech.teamninja.chatkittest.model;

/**
 * Created by tahmid on 29-Sep-17.
 */

public class Question {

    private String body;
    private String opA, opB, opC, opD;
    private String correctAnswer;
    private String correctOption;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getOpA() {
        return opA;
    }

    public void setOpA(String opA) {
        this.opA = opA;
    }

    public String getOpB() {
        return opB;
    }

    public void setOpB(String opB) {
        this.opB = opB;
    }

    public String getOpC() {
        return opC;
    }

    public void setOpC(String opC) {
        this.opC = opC;
    }

    public String getOpD() {
        return opD;
    }

    public void setOpD(String opD) {
        this.opD = opD;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public String getCorrectOption() {
        return correctOption;
    }

    public void setCorrectOption(String correctOption) {
        this.correctOption = correctOption;
    }

    public Question(String body, String opA, String opB, String opC, String opD) {

        this.body = body;

        this.opA = opA;
        this.opA = this.opA.replaceAll("Correct: ", "");
        if (opA.contains("Correct:")) {
            this.correctAnswer = opA.replaceAll("Correct: ", "");
            this.correctOption = "A";
        }

        this.opB = opB;
        this.opB = this.opB.replaceAll("Correct: ", "");
        if (opB.contains("Correct")) {
            this.correctAnswer = opB.replaceAll("Correct: ", "");
            this.correctOption = "B";
        }

        this.opC = opC;
        this.opC = this.opC.replaceAll("Correct: ", "");
        if (opC.contains("Correct")) {
            this.correctAnswer = opC.replaceAll("Correct: ", "");
            this.correctOption = "C";
        }

        this.opD = opD;
        this.opD = this.opD.replaceAll("Correct: ", "");
        if (opD.contains("Correct")) {
            this.correctAnswer = opD.replaceAll("Correct: ", "");
            this.correctOption = "D";
        }
    }

    @Override
    public String toString() {
        String body = this.body;

        String opa = this.opA;
        String opb = this.opB;
        String opc = this.opC;
        String opd = this.opD;

        return body + "\n\nA. " + opa + "\nB. " + opb + "\nC. " + opc + "\nD. " + opd;

    }
}
