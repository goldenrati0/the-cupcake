package tech.teamninja.chatkittest;

import android.os.CountDownTimer;

import com.stfalcon.chatkit.messages.MessagesListAdapter;

import tech.teamninja.chatkittest.model.Message;

/**
 * Created by tahmid on 27-Sep-17.
 */

public class Execution extends CountDownTimer {

    private MessagesListAdapter<Message> messageMessagesListAdapter;
    private Message message;

    /**
     * @param millisInFuture    The number of millis in the future from the call
     *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
     *                          is called.
     * @param countDownInterval The interval along the way to receive
     *                          {@link #onTick(long)} callbacks.
     */
    public Execution(long millisInFuture, long countDownInterval, MessagesListAdapter<Message> messageMessagesListAdapter, Message message) {
        super(millisInFuture, countDownInterval);
        this.messageMessagesListAdapter = messageMessagesListAdapter;
        this.message = message;
    }

    /**
     * Callback fired on regular interval.
     *
     * @param millisUntilFinished The amount of time until finished.
     */
    @Override
    public void onTick(long millisUntilFinished) {
        System.out.println("Wait...");
    }

    /**
     * Callback fired when the time is up.
     */
    @Override
    public void onFinish() {
        messageMessagesListAdapter.addToStart(message, true);
    }
}
