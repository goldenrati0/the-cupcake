package tech.teamninja.chatkittest;

import android.os.CountDownTimer;

import com.stfalcon.chatkit.messages.MessagesListAdapter;

import tech.teamninja.chatkittest.model.Message;

/**
 * Created by tahmid on 29-Sep-17.
 */

public class TemporaryExecution extends CountDownTimer {

    private String response;
    private MessagesListAdapter<Message> messageMessagesListAdapter;
    private Message message;

    public TemporaryExecution(long millisInFuture, long countDownInterval, String response, MessagesListAdapter<Message> messageMessagesListAdapter, Message message) {
        super(millisInFuture, countDownInterval);
        this.response = response;
        this.messageMessagesListAdapter = messageMessagesListAdapter;
        this.message = message;
    }

    @Override
    public void onTick(long millisUntilFinished) {

    }

    @Override
    public void onFinish() {
        messageMessagesListAdapter.addToStart(message, true);
    }
}
