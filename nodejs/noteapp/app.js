const _ = require('lodash');
const yargs = require('yargs');

const notes = require('./notes.js');

const argv = yargs
    .command('add', 'Add a new note', {
        title: {
            describe: 'Title of the note',
            demand: true,
            alias: 't'
        },
        body: {
            describe: 'Body of the note',
            demand: true,
            alias: 'b'
        }
    })
    .command('read', 'Read a note', {
        title: {
            describe: 'Title of the note you want to read',
            demand: true,
            alias: 't'
        }
    })
    .command('remove', 'Remove a note', {
        title: {
            describe: 'Title of the note you want to remove',
            demand: true,
            alias: 't'
        }
    })
    .command('list', 'List all notes')
    .help()
    .argv

var command = argv._[0];

if (command === 'add') {
    notes.addNote(argv.title, argv.body);
} else if (command === 'list') {
    notes.getAll();
} else if (command === 'remove') {
    notes.removeNote(argv.title);
} else if (command === 'read') {
    notes.readNote(argv.title);
} else {
    console.log('Invalid command');
}